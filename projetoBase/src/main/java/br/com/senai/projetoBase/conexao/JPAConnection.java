package br.com.senai.projetoBase.conexao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**Classe que contém métodos de conexão ao banco de dados para todos os métodos de persistencia utilizados no projeto.
 * 
 * @version 1.0.0
 * @since 1.0.0 
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class JPAConnection {

	/**
	 * Parâmetro do tipo EntityManagerFactory, utilizado para gerar instancias da classe EntityManager, responsável por gerenciar e manipular entidades para a persistência de dados.
	 * 
	 */
	private static EntityManagerFactory emf;
	
	/**
	 *  Parâmetro do tipo EntityManager utilizado para configurar as ações tomadas pelos métodos de persistência de dados.
	 * 
	 */
	private EntityManager em;
	
	/**Método utilizado para configurar a unidade de persistencia que gerencia as operações sobre as entidades do projeto.
	 * 
	 * @return emf
	 * @see {@link #emf}
	 */
	private EntityManagerFactory getEntityManagerFactory(){
		if(emf == null || !emf.isOpen()){
			emf = Persistence.createEntityManagerFactory("projetoBase");
		}
		return emf;
	}
	
	/**Método utilizado para criar a unidade de persistencia.
	 * 
	 * @see #getEntityManagerFactory
	 * 
	 * @see {@link #em}
	 * 
	 * @return em
	 */
	protected EntityManager getEntityManager(){
		if(em == null || !em.isOpen()){
			em = getEntityManagerFactory().createEntityManager();
		}
		return em;
	}
	
	
	/**Método utilizado para encerrar a conexão com a unidade de persistência.
	 *
	 */
	protected void closeEntityManager(){
		getEntityManager().close();
		em = null;
	}
	
	/**Método utilizado para iniciar a conexão com a unidade de persistência.
	 *
	 */
	protected void beginTransaction(){
		getEntityManager().getTransaction().begin();
	}
	
	/**Método utilizado para enviar a operação feita para a unidade de persistência.
	 *
	 */
	protected void commit(){
		getEntityManager().getTransaction().commit();
	}
	
	/**Método utilizado para desfazer a operação feita com o método commit caso ocorra algum erro na comunicação com a
	 * unidade de persistência.
	 *
	 *@see #commit
	 */
	protected void rollback(){
		getEntityManager().getTransaction().rollback();
	}
}