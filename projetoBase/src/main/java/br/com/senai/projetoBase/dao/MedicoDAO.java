package br.com.senai.projetoBase.dao;

import java.util.List;

import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Medico;
import br.com.senai.projetoBase.model.Paciente;
/**Interface que contém métodos específicos para a entidade Médico.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public interface MedicoDAO extends CrudDAO<Medico,Integer>{

	/**Método utilizado para retornar uma lista com todos os médicos que contém o parâmetro específicado.
	 * 
	 * @return List<Medico>
	 */
	List<Medico> listarMedicosAC(String medico) throws MyException;
	
}
