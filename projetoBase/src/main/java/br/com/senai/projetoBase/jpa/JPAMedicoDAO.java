
package br.com.senai.projetoBase.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.senai.projetoBase.abs.JPAAbstract;
import br.com.senai.projetoBase.dao.MedicoDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Medico;

/**Classe que que utiliza os métodos específicos de persistência a entidade Médico
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class JPAMedicoDAO extends JPAAbstract<Medico,Integer> implements MedicoDAO{

	@Override
	public List<Medico> listarMedicosAC(String medico) throws MyException {
		List<Medico> medicos = null;
		try {
			EntityManager em = getEntityManager();
			TypedQuery<Medico> query = em.createQuery("SELECT m FROM Medico m WHERE upper(m.medico) LIKE upper(:medico)", Medico.class);
			medico = "%" + medico + "%";
			query.setParameter("medico", medico);
			medicos = query.getResultList();
		} catch (Exception e) {
			throw new MyException(e);
		} finally {
			closeEntityManager();
		}
		return medicos;
	}

}
