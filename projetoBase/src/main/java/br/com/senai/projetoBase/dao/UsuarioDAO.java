package br.com.senai.projetoBase.dao;

import java.util.List;

import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Usuario;
/**Interface que contém métodos específicos para a entidade Usuario.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public interface UsuarioDAO extends CrudDAO<Usuario,Integer>{
	/**Método utilizado para verificar no banco de as informações sobre login/senha estão corretos.
	 * 
	 * @return boolean
	 */
	public boolean findUser(String usuario, String senha) throws MyException;


	
}

