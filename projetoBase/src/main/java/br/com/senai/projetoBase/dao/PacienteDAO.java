package br.com.senai.projetoBase.dao;

import java.util.List;

import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Paciente;

/**Interface que contém métodos específicos para a entidade Paciente.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public interface PacienteDAO extends CrudDAO<Paciente,Integer>{

	/**Método utilizado para retornar uma lista com todos os pacientes que contém o parâmetro específicado.
	 * 
	 * @return List<Paciente>
	 */
	List<Paciente> listarPacientesAC(String paciente) throws MyException;
	
}
