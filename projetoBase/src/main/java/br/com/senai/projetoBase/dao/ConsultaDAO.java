package br.com.senai.projetoBase.dao;

import java.util.List;

import br.com.senai.projetoBase.model.Consulta;


/**Interface que contém métodos específicos para a entidade Consulta.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public interface ConsultaDAO extends CrudDAO<Consulta,Integer>{

	/**Método utilizado para retornar uma lista com todas as consultas/agendamentos existentes no sistema.
	 * 
	 * @return List<Consulta>
	 */
	List<Consulta> buscarConsultas();

}
