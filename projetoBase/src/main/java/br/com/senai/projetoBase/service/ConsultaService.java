package br.com.senai.projetoBase.service;

import java.util.List;

import br.com.senai.projetoBase.dao.ConsultaDAO;
import br.com.senai.projetoBase.dao.MedicoDAO;
import br.com.senai.projetoBase.dao.PacienteDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.factory.DAOFactory;
import br.com.senai.projetoBase.model.Consulta;
import br.com.senai.projetoBase.model.Medico;
import br.com.senai.projetoBase.model.Paciente;

/**Classe responsável por efetuar a troca de informações sobre a entidade Consulta entre a camada REST e o servidor.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class ConsultaService {

	public Consulta save(Consulta c) throws MyException {
		//É passado para o objeto persist a instancia do método static getInstanceOf(), que retorna uma interface que contém acesso 
		//aos métodos de persistencia

		
		ConsultaDAO persist= (ConsultaDAO) DAOFactory.getInstanceOf(ConsultaDAO.class);
		
		if(c.getId()==0){
			return persist.incluir(c);
		}else{
			return persist.atualizar(c);
		}
		
	}

	public List<Consulta> list() throws MyException {
		ConsultaDAO persist= (ConsultaDAO) DAOFactory.getInstanceOf(ConsultaDAO.class);
		return persist.buscarConsultas();
	}

	public Consulta listID(int id) throws MyException {
		ConsultaDAO persist= (ConsultaDAO) DAOFactory.getInstanceOf(ConsultaDAO.class);
		return persist.buscarPorId(id);
	}

	public Consulta delete(int id) throws MyException {
		ConsultaDAO persist= (ConsultaDAO) DAOFactory.getInstanceOf(ConsultaDAO.class);
		return persist.deletar(id);
	}

}
