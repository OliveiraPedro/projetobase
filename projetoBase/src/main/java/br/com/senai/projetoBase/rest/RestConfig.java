package br.com.senai.projetoBase.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**Classe responsável por configurar um ponto de acesso seguro aos recursos do sistema.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@ApplicationPath("rest")
public class RestConfig extends Application {

}
