package br.com.senai.projetoBase.factory;

import br.com.senai.projetoBase.dao.ConsultaDAO;
import br.com.senai.projetoBase.dao.MedicoDAO;
import br.com.senai.projetoBase.dao.PacienteDAO;
import br.com.senai.projetoBase.dao.UsuarioDAO;
import br.com.senai.projetoBase.jpa.JPAConsultaDAO;
import br.com.senai.projetoBase.jpa.JPAMedicoDAO;
import br.com.senai.projetoBase.jpa.JPAPacienteDAO;
import br.com.senai.projetoBase.jpa.JPAUsuarioDAO;

/**Classe utilizada para gerar direrentes instâncias de outros Objetos.
 * Neste caso retorna uma instância de classe JPA, o que facilita o acesso aos métodos de persistência.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class DAOFactory {
	
	
	/**Método que verifica o tipo da instancia que está recebendo como parâmetro
	 *
	 *@param c - Classe recebida como parâmetro que o tipo da instância que a factory deve gerar.
	 */
	@SuppressWarnings("rawtypes")
	public static Object getInstanceOf(Class c) {
		if ( c.equals(PacienteDAO.class) ) {
			return new JPAPacienteDAO();
		}else if ( c.equals(MedicoDAO.class) ) {
			return new JPAMedicoDAO();
		}else if ( c.equals(ConsultaDAO.class) ) {
			return new JPAConsultaDAO();
		}
		else if ( c.equals(UsuarioDAO.class) ) {
			return new JPAUsuarioDAO();
		}
		return null;
	}
}
