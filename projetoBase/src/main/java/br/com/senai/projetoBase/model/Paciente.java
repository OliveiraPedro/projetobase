package br.com.senai.projetoBase.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**Entidade que contém todas as informações relacionadas a um paciente.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Entity
@Table(name = "pacientes")
public class Paciente {

	public Paciente() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "cpf",nullable=true)
	private String cpf;

	@Column(name = "paciente", nullable = false)
	private String paciente;

	@Column(name = "logradouro", nullable = false)
	private String logradouro;

	@Column(name = "nascimento", nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar nascimento;
	
	@Column(name = "telefone", nullable = false)
	private long telefone;

	/**Método que retorna o nome do paciente.
	 * 
	 * @return paciente 
	 */
	public String getPaciente() {
		return paciente;
	}

	/**Método que define o valor retornado no método getPaciente()
	 * 
	 * @see #getPaciente
	 * 
	 */
	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	/**Método que retorna a identificação do paciente.
	 * 
	 * @return id 
	 */
	public int getId() {
		return id;
	}

	/**Método que define o valor retornado no método getId()
	 * 
	 * @see #getId
	 * 
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**Método que retorna o cpf do paciente.
	 * 
	 * @return cpf 
	 */
	public String getCpf() {
		return cpf;
	}

	/**Método que define o valor retornado no método getCpf()
	 * 
	 * @see #getCpf
	 * 
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**Método que retorna o logradouro do paciente.
	 * 
	 * @return logradouro 
	 */
	public String getLogradouro() {
		return logradouro;
	}

	/**Método que define o valor retornado no método getLogradouro()
	 * 
	 * @see #getLogradouro
	 * 
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	/**Método que retorna a data de nascimento do paciente.
	 * 
	 * @return nascimento 
	 */
	public Calendar getNascimento() {
		return nascimento;
	}

	/**Método que define o valor retornado no método getNascimento()
	 * 
	 * @see #getNascimento
	 * 
	 */
	public void setNascimento(Calendar nascimento) {
		this.nascimento = nascimento;
	}

	/**Método que retorna o telefone do paciente.
	 * 
	 * @return telefone 
	 */
	public long getTelefone() {
		return telefone;
	}

	/**Método que define o valor retornado no método getTelefone()
	 * 
	 * @see #getTelefone
	 * 
	 */
	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}
}
