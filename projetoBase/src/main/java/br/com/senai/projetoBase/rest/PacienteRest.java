package br.com.senai.projetoBase.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.senai.projetoBase.filter.Secured;
import br.com.senai.projetoBase.model.Paciente;
import br.com.senai.projetoBase.service.PacienteService;


/**Classe REST que define o estilo de arquitetura cliente-servidor para a entidade Paciente.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Api
@Path("pacientes")
@Consumes("application/json")
@Produces("application/json")
public class PacienteRest extends UtilRest{
	
	@ApiOperation(response = Paciente.class,value = "Cadastrar (CREATE) um novo paciente", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Paciente.class, code = 200, message = "Registro efetuado com sucesso."),
				   @ApiResponse(response = Paciente.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Paciente.class, code = 500, message = "Falha no servidor.")})
	
	@POST
	@Secured
	public Response registrarPaciente(@ApiParam(value="Objeto contendo informações para efetuar o cadastro de um paciente", name="json", required=true)String json){
		
		try{
			Paciente paciente = getObjectMapper().readValue(json, Paciente.class);
			PacienteService service = new PacienteService();		
			return buildSaveResponse(service.save(paciente));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Paciente.class,value = "Recuperar (READ) as informações dos pacientes que estão no sistema",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Paciente.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Paciente.class, code = 500, message = "Não foi possível efetuar sua requisição.")})
	
	@GET
	@Secured
	public Response listarPacientes(){
		
		try{
			PacienteService service = new PacienteService();		
			return buildGetResponse(service.list());
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Paciente.class,value = "Recuperar (READ) informações específicas sobre um paciente em sua entidade.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Paciente.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Paciente.class, code = 500, message = "Falha no servidor.")})
	
	@GET
	@Secured
	@Path("/{id}")
	public Response listarPacientesID(@ApiParam(value="Identificação recebida como parâmetro para efetuar uma busca específica na entidade Paciente", name="id", required=true)@PathParam("id")int id){
		
		try{
			PacienteService service = new PacienteService();
			return buildGetResponse(service.listID(id));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Paciente.class,value = "Atualizar (UPDATE) as informações de um paciente no sistema", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Paciente.class, code = 200, message = "Registro atualizado com sucesso."),
				   @ApiResponse(response = Paciente.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Paciente.class, code = 500, message = "Falha no servidor.")})

	@PUT
	@Secured
	@Path("/{id}")
	public Response editarPaciente(@ApiParam(value="Objeto contendo informações para efetuar a alteração de um paciente", name="json", required=true)String json){
		
		try{
			Paciente paciente = getObjectMapper().readValue(json, Paciente.class);
			PacienteService service = new PacienteService();
			return buildUpdateResponse(service.update(paciente));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Paciente.class,value = "Remover (REMOVE) informações sobre um paciente do sistema.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Paciente.class, code = 200, message = "Registro excluído com sucesso."),
				   @ApiResponse(response = Paciente.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Paciente.class, code = 500, message = "Falha no servidor.")})
	
	@DELETE
	@Secured
	@Path("/{id}")
	public Response removerPaciente(@ApiParam(value="Identificação recebida como parâmetro para remover um paciente do sistema", name="id", required=true)@PathParam("id")int id){
		
		try{
			PacienteService service = new PacienteService();
			return buildRemoveResponse(service.delete(id));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}

}
