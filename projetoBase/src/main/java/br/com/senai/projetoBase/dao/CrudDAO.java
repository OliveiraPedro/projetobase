package br.com.senai.projetoBase.dao;

import java.util.List;

import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Paciente;

/**Inteface que contém métodos genericos para a persistência de dados.
 * 
 * @version 1.0.0
 * @since 1.0.0
 *  
 * @author pedro_campelo
 *
 * @param <T>
 * @param <ID>
 */
public interface CrudDAO<T,ID> {
	
	/**Método responsável por fazer a inserção de dados(CREATE) no banco de dados.
	 * @param t - Entidade generica já populada com informações para efetuar a inserção de dados.
	 *@return t - Retorna a entidade contendo os dados inseridos no banco de dados.
	 */
	public T incluir(T t) throws MyException;
	
	/**Método responsável por fazer a busca especifíca por um resultado na entidade recebida por parametro.
	 * @param id - Valor genérico que recebe por parametro o tipo de identificação utilizada na entidade, facilitando a busca de determinado valor.
	 *@return t - Retorna a entidade contendo os dados recuperados do banco de dados conforme a identificação que recebeu.
	 */
	public T buscarPorId(ID id) throws MyException;
	
	/**Método responsável por fazer a alteração de dados(UPDATE) no banco de dados conforme parametros contidos na entidade.
	 * @param t - Entidade generica já populada com informações para efetuar a alteração de dados.
	 * @return t - Retorna a entidade contendo os dados que foram enviados para a atualização do banco de dados.
	 */
	public T atualizar(T t) throws MyException;
	
	/**Método responsável por remover(DELETE) um resultado ligado a entidade especificada conforme o valor de sua identificação.
	 * @param id - Valor genérico que recebe por parametro o tipo de identificação utilizada na entidade, facilitando a remoção de determinado valor.
	 * @return t - Retorna algumas informações sobre a entidade que foi excluída do banco de dados.
	 */
	public T deletar(ID id) throws MyException;
	
	/**Método responsável por recuperar(READ) todos os resultados ligados a entidade especificada.
	 * Retorna a entidade contendo todos os dados recuperados do banco de dados.
	 */
	public List<T> buscarTodos() throws MyException;
}
