package br.com.senai.projetoBase.filter;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.METHOD;

//@NameBinding - Anotação utilizada para defifinir configuração de requisições via confirmação de token
/**Classe responsável por determinar quais recursos devem ser acessados somente por um usuário com autorização válida.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@NameBinding
@Retention(RUNTIME)
@Target({TYPE,METHOD})
public @interface Secured {

}
