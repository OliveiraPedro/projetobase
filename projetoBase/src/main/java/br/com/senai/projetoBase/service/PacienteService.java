package br.com.senai.projetoBase.service;

import java.util.List;

import br.com.senai.projetoBase.dao.PacienteDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.factory.DAOFactory;
import br.com.senai.projetoBase.model.Paciente;

/**Classe responsável por efetuar a troca de informações sobre a entidade Paciente entre a camada REST e o servidor.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class PacienteService {

	
	public Paciente save(Paciente p) throws MyException {
	
		//É passado para o objeto persist a instancia do método static getInstanceOf(), que retorna uma interface que contém acesso 
		//aos métodos de persistencia
		PacienteDAO persist= (PacienteDAO) DAOFactory.getInstanceOf(PacienteDAO.class);
		return persist.incluir(p);
	}

	public List<Paciente> list() throws MyException {
		PacienteDAO persist= (PacienteDAO) DAOFactory.getInstanceOf(PacienteDAO.class);

		System.out.println(persist);
		return persist.buscarTodos();
	}

	public Paciente listID(int id) throws MyException {
		PacienteDAO persist= (PacienteDAO) DAOFactory.getInstanceOf(PacienteDAO.class);
		return persist.buscarPorId(id);
	}

	public Paciente update(Paciente p) throws MyException {
		PacienteDAO persist= (PacienteDAO) DAOFactory.getInstanceOf(PacienteDAO.class);
		return persist.atualizar(p);
	}

	public Paciente delete(int id) throws MyException {
		PacienteDAO persist= (PacienteDAO) DAOFactory.getInstanceOf(PacienteDAO.class);
		return persist.deletar(id);
	}

	public List<Paciente> listPatient(String paciente) throws MyException {
		PacienteDAO persist= (PacienteDAO) DAOFactory.getInstanceOf(PacienteDAO.class);
		return persist.listarPacientesAC(paciente);
	}
}
