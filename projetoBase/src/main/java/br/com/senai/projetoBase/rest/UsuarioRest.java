package br.com.senai.projetoBase.rest;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.senai.projetoBase.model.Usuario;
import br.com.senai.projetoBase.service.UsuarioService;


/**Classe REST que define o estilo de arquitetura cliente-servidor para a entidade Usuário.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Api
@Path("usuarios")
@Consumes("application/json")
@Produces("application/json")
public class UsuarioRest extends UtilRest{
	
	@ApiOperation(response = Usuario.class,value = "Cadastrar (CREATE) um novo usuario", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Usuario.class, code = 200, message = "Registro efetuado com sucesso."),
				   @ApiResponse(response = Usuario.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Usuario.class, code = 500, message = "Não foi possível efetuar sua requisição.")})
	@POST
	public Response registrarUsuario(@ApiParam(value="Objeto contendo valores de usuario e senha para cadastro", name="json", required=true) String json){
		
		
		try{
			Usuario usuario = getObjectMapper().readValue(json, Usuario.class);
			UsuarioService service = new UsuarioService();		
			return buildSaveResponse(service.save(usuario));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
}
