package br.com.senai.projetoBase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**Entidade que contém todas as informações relacionadas a um usuário.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Entity
@Table(name="usuarios")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name="usuario")
	private String usuario;
	
	@Column(name="senha")
	private String senha;

	/**Método que retorna a identificação do paciente.
	 * 
	 * @return id 
	 */
	public int getId() {
		return id;
	}

	/**Método que define o valor retornado no método getId()
	 * 
	 * @see #getId
	 * 
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**Método que retorna o login do usuário informado na página de entrada.
	 * 
	 * @return usuario 
	 */
	public String getUsuario() {
		return usuario;
	}

	/**Método que define o valor retornado no método getUsuario()
	 * 
	 * @see #getUsuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**Método que retorna a senha do usuário informada na página de entrada.
	 * 
	 * @return senha 
	 */
	public String getSenha() {
		return senha;
	}

	/**Método que define o valor retornado no método getSenha()
	 * 
	 * @see #getSenha
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

}
