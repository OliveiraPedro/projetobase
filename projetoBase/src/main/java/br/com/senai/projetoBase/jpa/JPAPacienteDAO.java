
package br.com.senai.projetoBase.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.senai.projetoBase.abs.JPAAbstract;
import br.com.senai.projetoBase.dao.PacienteDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Paciente;
/**Classe que que utiliza os métodos específicos de persistência a entidade Paciente
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class JPAPacienteDAO extends JPAAbstract<Paciente,Integer> implements PacienteDAO{

	@Override
	public List<Paciente> listarPacientesAC(String paciente) throws MyException {
		List<Paciente> pacientes = null;
		try {
			EntityManager em = getEntityManager();
			TypedQuery<Paciente> query = em.createQuery("SELECT p FROM Paciente p WHERE upper(p.paciente) LIKE upper(:paciente)", Paciente.class);
			paciente = "%" + paciente + "%";
			query.setParameter("paciente", paciente);
			pacientes = query.getResultList();
		} catch (Exception e) {
			throw new MyException(e);
		} finally {
			closeEntityManager();
		}
		return pacientes;
	}


}
