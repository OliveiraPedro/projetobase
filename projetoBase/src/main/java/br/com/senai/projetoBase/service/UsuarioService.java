package br.com.senai.projetoBase.service;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


import br.com.senai.projetoBase.dao.UsuarioDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.factory.DAOFactory;
import br.com.senai.projetoBase.model.Usuario;
import br.com.senai.projetoBase.rest.UsuarioRest;

/**Classe responsável por efetuar a troca de informações sobre a entidade Usuário entre a camada REST e o servidor.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@ApplicationPath("/v1")
public class UsuarioService extends Application{
	
	
	public boolean findUser(String usuario,String senha) throws MyException{
		
		UsuarioDAO usuarioDAO=(UsuarioDAO) DAOFactory.getInstanceOf(UsuarioDAO.class);
		return usuarioDAO.findUser(usuario,senha);
	}

	public Usuario save(Usuario usuario) throws MyException {
		UsuarioDAO usuarioDAO=(UsuarioDAO) DAOFactory.getInstanceOf(UsuarioDAO.class);
		return usuarioDAO.incluir(usuario);
	}


}
