package br.com.senai.projetoBase.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;











import br.com.senai.projetoBase.filter.Secured;
import br.com.senai.projetoBase.model.Medico;
import br.com.senai.projetoBase.service.MedicoService;

/**Classe REST que define o estilo de arquitetura cliente-servidor para a entidade Médico.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Api
@Path("medicos")
@Consumes("application/json")
@Produces("application/json")
public class MedicoRest extends UtilRest{
	
	@ApiOperation(response = Medico.class,value = "Cadastrar (CREATE) um novo médico", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Medico.class, code = 200, message = "Registro efetuado com sucesso."),
				   @ApiResponse(response = Medico.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Medico.class, code = 500, message = "Falha no servidor.")})
	
	@POST
	@Secured
	public Response registrarMedico(@ApiParam(value="Objeto contendo informações para efetuar o cadastro de um médico", name="json", required=true)String json){
		
		try{
			Medico medico = getObjectMapper().readValue(json, Medico.class);
			MedicoService service = new MedicoService();
			
			return buildSaveResponse(service.save(medico));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	
	@ApiOperation(response = Medico.class,value = "Recuperar (READ) as informações dos médicos que estão no sistema",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Medico.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Medico.class, code = 500, message = "Não foi possível efetuar sua requisição.")})
	
	@GET
	@Secured
	public Response listarMedicos(){
		
		try{
			MedicoService service = new MedicoService();
			
			return buildGetResponse(service.list());
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Medico.class,value = "Recuperar (READ) informações específicas sobre um médico em sua entidade.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Medico.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Medico.class, code = 500, message = "Falha no servidor.")})
	
	@GET
	@Secured
	@Path("/{id}")
	public Response listarMedicosID(@ApiParam(value="Identificação recebida como parâmetro para efetuar uma busca específica na entidade Medico", name="id", required=true)@PathParam("id") int id){
		
		try{
			MedicoService service = new MedicoService();
			
			return buildGetResponse(service.listID(id));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Medico.class,value = "Atualizar (UPDATE) as informações de um médico no sistema", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Medico.class, code = 200, message = "Registro atualizado com sucesso."),
				   @ApiResponse(response = Medico.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Medico.class, code = 500, message = "Falha no servidor.")})

	@PUT
	@Secured
	@Path("/{id}")
	public Response editarMedico(@ApiParam(value="Objeto contendo informações para efetuar a alteração de um médico", name="json", required=true)String json){
		
		try{
			Medico medico = getObjectMapper().readValue(json, Medico.class);
			MedicoService service = new MedicoService();
			
			return buildUpdateResponse(service.update(medico));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Medico.class,value = "Remover (REMOVE) informações sobre um médico do sistema.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Medico.class, code = 200, message = "Registro excluído com sucesso."),
				   @ApiResponse(response = Medico.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Medico.class, code = 500, message = "Falha no servidor.")})
	
	@DELETE
	@Secured
	@Path("/{id}")
	public Response removerMedico(@ApiParam(value="Identificação recebida como parâmetro para remover um médico do sistema", name="id", required=true)@PathParam("id")int id){
		
		try{
			MedicoService service = new MedicoService();
			
			return buildRemoveResponse(service.delete(id));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}

}
