package br.com.senai.projetoBase.jpa;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.senai.projetoBase.abs.JPAAbstract;
import br.com.senai.projetoBase.dao.ConsultaDAO;
import br.com.senai.projetoBase.model.Consulta;

/**Classe que que utiliza os métodos específicos de persistência a entidade Consulta.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class JPAConsultaDAO extends JPAAbstract<Consulta,Integer> implements ConsultaDAO{
	@Override
	public List<Consulta> buscarConsultas(){
		
		Query consulta=getEntityManager().createQuery("SELECT q from Consulta q");
		List<Consulta> retorno=consulta.getResultList();
		return retorno;
	};


}
