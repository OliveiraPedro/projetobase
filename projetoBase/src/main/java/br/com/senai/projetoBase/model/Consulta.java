package br.com.senai.projetoBase.model;

import java.sql.Time;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**Entidade que contém todas as informações relacionadas a uma consulta.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Entity
@Table(name="consultas")
public class Consulta {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn
	private Paciente paciente;
	
	@ManyToOne
	@JoinColumn
	private Medico medico;
	
	private String hora;
	
	
	@Temporal(TemporalType.DATE)
	private Calendar data_consulta;


	/**Método que retorna o horário em que a consulta foi marcada.
	 * 
	 * @return hora 
	 */
	public String getHora() {
		return hora;
	}

	/**Método que define o valor retornado no método getHora()
	 * 
	 * @see #getHora
	 * 
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}

	/**Método que retorna a data em que a consulta foi marcada.
	 * 
	 * @return data_consulta 
	 */
	public Calendar getData_consulta() {
		return data_consulta;
	}

	/**Método que define o valor retornado no método getData_consulta()
	 * 
	 * @see #getData_consulta
	 * 
	 */
	public void setData_consulta(Calendar data_consulta) {
		this.data_consulta = data_consulta;
	}

	/**Método que retorna a identificação da consulta.
	 * 
	 * @return id 
	 */
	public int getId() {
		return id;
	}

	/**Método que define o valor retornado no método getId()
	 * 
	 * @see #getId
	 * 
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**Método que retorna as informações sobre o paciente que solicitou uma consulta.
	 * 
	 * @return id 
	 */
	public Paciente getPaciente() {
		return paciente;
	}

	/**Método que define o valor retornado no método getPaciente()
	 * 
	 * @see #getPaciente
	 * 
	 */
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	/**Método que retorna as informações sobre o medico que irá conduzir a consulta marcada.
	 * 
	 * @return id 
	 */
	public Medico getMedico() {
		return medico;
	}

	/**Método que define o valor retornado no método getMedico()
	 * 
	 * @see #getMedico
	 * 
	 */
	public void setMedico(Medico medico) {
		this.medico = medico;
	}

}
