package br.com.senai.projetoBase.exceptions;

/**Classe utilizada para efetuar o tratamento de erros que venham a acontecer durante o funcionamento da API.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class MyException extends Exception {
	
	/**Construtor responsável por chamar as configurações do construtor da classe pai, ou seja, para que seja possível fazer uso
	 *  de uma assinatura adequada para configurar uma mensagem .
	 * 
	 * Pode ser chamado com diversas implementações e com diferentes parâmetros na sua declaração.
	 */
	public MyException(){
		super();
	}
	
	/**Construtor responsável por chamar as configurações do construtor da classe pai, ou seja, para que seja possível fazer uso
	 *  de uma assinatura adequada para configurar uma mensagem .
	 * 
	 * @param mensagem - Recebe como parâmetro um texto que foi enviado no momento em for chamado pela declaração throw new.
	 */
	public MyException(String mensagem){
		super(mensagem);
	}
	
	/**Construtor responsável por chamar as configurações do construtor da classe pai, ou seja, para que seja possível fazer uso
	 *  de uma assinatura adequada para configurar uma mensagem .
	 * 
	 * @param mensagem - Recebe como parâmetro um texto que foi enviado no momento em for chamado pela declaração throw new.
	 * @param t - Recebe como parâmetro um objeto do tipo Throwable para repassar o erro gerado pelo servidor junto da mensagem personalizada.
	 */
	public MyException(String mensagem,Throwable t){
		super(mensagem,t);
	}
	
	/**Construtor responsável por chamar as configurações do construtor da classe pai, ou seja, para que seja possível fazer uso
	 *  de uma assinatura adequada para configurar uma mensagem .
	 *  
	 * @param t - Recebe como parâmetro um objeto do tipo Throwable para mostrar o erro gerado pelo servidor.
	 */
	public MyException(Throwable t){
		super(t);
	}

}
