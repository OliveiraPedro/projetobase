package br.com.senai.projetoBase.rest;

import java.io.IOException;
import java.util.HashMap;

import javax.security.auth.login.LoginException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.internal.com.fasterxml.jackson.core.JsonParseException;
import com.auth0.jwt.internal.com.fasterxml.jackson.databind.JsonMappingException;

import org.codehaus.jackson.map.ObjectMapper;

import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.exceptions.MyLoginException;
import br.com.senai.projetoBase.model.Usuario;
import br.com.senai.projetoBase.service.UsuarioService;

/**Classe responsável por cuidar da autenticação de usuário/senha do sistema, álem de gerar o token para efetuar requisições.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Path("endpoint")
@Produces("application/json")
@Consumes("application/json")
public class Endpoint extends UtilRest{

	/**Método responsável por fazer validar as informações de usuário/senha.
	 * @param json - Objeto enviado via requisição solicitada pelo navegador contendo as informações do formulário de login.
	 *@return Response - Retorna um objeto do tipo Response,seja ele com sucesso ou com erro sobre a operação efetuada.
	 */
	@POST
	public Response authenticateUser(String json) throws JsonParseException, JsonMappingException, IOException {


		String secure="";
		Usuario usuario=new ObjectMapper().readValue(json,Usuario.class);		
		try {
			System.out.println(json);
			String user=usuario.getUsuario();
			String senha=usuario.getSenha();
			// Verifica se as informações que o usuario preencheu são válidas
			if(searchLogin(user, senha)){
				System.out.println("deu certo");


				//Gera um token/identificação para o usuário
				String token = issueToken(user+senha);

				secure="{\"token\":\""+token+"\"}";

				System.out.println("teste");

				// Return the token on the response
				return Response.ok(secure).build();
			}else{
				System.out.println("não deu certo");
				return buildErrorResponse(new MyLoginException(), Response.Status.UNAUTHORIZED);
			}

			/*if(user.equals("admin") && senha.equals("admin")){

				
			}else{
			
				return buildErrorResponse(new MyLoginException(), Response.Status.UNAUTHORIZED);
			}*/

			

	}catch (Exception e) {
		return Response.status(Response.Status.UNAUTHORIZED).build();
	}
}

	/**Método responsável por buscar no banco de dados informações da entidade Usuário compativeis com as que foram preenchidas
	 * no formulário de login.
	 * @param username - Usuário recebido via parametro pelo método de autenticação.
	 * @param password  - Senha recebida via parametro pelo método de autenticação.
	 *@return boolean - Retorna uma resposta verdadeira ou falsa(true/false) conforme o retorno do servidor.
	 */
	private boolean searchLogin(String username, String password) throws MyException {
		UsuarioService uService=new UsuarioService();

		return uService.findUser(username,password);

	}

	/**Método responsável por gerar o token que permitirá ao usuário efetuar requisições.
	 * @param username - Usuário recebido via parametro pelo método de autenticação para gerar o token.
	 *@return claims - Retorna o token gerado que permite requisições seguras ao servidor.
	 */
	private String issueToken(String username) {
		final String secret = "dinheiro";

		final long iat = System.currentTimeMillis() / 1000L; // issued at claim 
		final long exp = iat + 60L; // expires claim. In this case the token expires in 60 seconds

		final JWTSigner signer = new JWTSigner(secret);
		final HashMap<String, Object> claims = new HashMap<String, Object>();
		claims.put("user", username);
		claims.put("iat", iat);
		claims.put("exp", exp);

		return signer.sign(claims);
	}

}
