package br.com.senai.projetoBase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**Entidade que contém todas as informações relacionadas a um medico.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Entity
@Table(name = "medicos")
public class Medico {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "medico",nullable=false)
	private String medico;
	
	@Column(name = "crm",nullable=false)
	private String crm;
	
	@Column(name = "telefone",nullable=false)
	private long telefone;
	
	
	/**Método que retorna o nome do medico.
	 * 
	 * @return medico 
	 */
	public String getMedico() {
		return medico;
	}

	/**Método que define o valor retornado no método getMedico()
	 * 
	 * @see #getMedico
	 * 
	 */
	public void setMedico(String medico) {
		this.medico = medico;
	}

	/**Método que retorna a identificação do medico.
	 * 
	 * @return id 
	 */
	public int getId() {
		return id;
	}

	/**Método que define o valor retornado no método getId()
	 * 
	 * @see #getId
	 * 
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**Método que retorna o crm do medico.
	 * 
	 * @return crm 
	 */
	public String getCrm() {
		return crm;
	}

	/**Método que define o valor retornado no método getCrm()
	 * 
	 * @see #getCrm
	 * 
	 */
	public void setCrm(String crm) {
		this.crm = crm;
	}

	/**Método que retorna o telefone do medico.
	 * 
	 * @return telefone 
	 */
	public long getTelefone() {
		return telefone;
	}

	/**Método que define o valor retornado no método getTelefone()
	 * 
	 * @see #getTelefone
	 * 
	 */
	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

}
