package br.com.senai.projetoBase.abs;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.senai.projetoBase.conexao.JPAConnection;
import br.com.senai.projetoBase.dao.CrudDAO;
import br.com.senai.projetoBase.exceptions.MyException;




/**Classe que contém métodos padrões de persistência ao banco de dados.Ao todos são cinco métodos, sendo que um deles efetua
 * busca especifica pela entidade, recebendo como parametro a identificação(ID) da mesma.
 *  @see #buscarPorId
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 * 
 * @version 1.0.0
 * @since 1.0.0
 *
 * @param <T> - Entidade generica utilizada para efetuar a persistência de dados.
 * @param <ID> - Identificação da entidade generica recebida. Por ser generica , pode receber de maneira dinamica diferentes tipos de identificação.
 */
public abstract class JPAAbstract<T,ID> extends JPAConnection implements CrudDAO<T,ID>{
	// este método será uma junção de prepareStatementGerandoId, e dos
	
	/** Parametro que recebe via espelhamento no construtor a entidade especificada
	 * na declaração da classe
	 */
	protected Class<T> entidade;

	
	
	/**Construtor responsável por efetuar o espelhamento da entidade que é utilizada na persistencia ao banco de dados.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public JPAAbstract(){
		ParameterizedType superclass = (ParameterizedType)getClass().getGenericSuperclass();
		entidade=(Class<T>)superclass.getActualTypeArguments()[0];
	}
	
	public T incluir(T t) throws MyException {
		try {
			EntityManager em = getEntityManager();
			beginTransaction();
			em.persist(t);
			commit();
		} catch (Exception e) {
			rollback();
			throw new MyException(e);
		} finally {
			closeEntityManager();
		}
		return t;
	}
	
	public T buscarPorId(ID id)throws MyException{
		return getEntityManager().find(entidade, id);
	}

	public T atualizar(T t)throws MyException{
		//A instancia em da classe EntityManager, recebe o método getEntityManager(), herdado da classe JPAConnection, para que
		//possa atualizar o banco de dados com o método merge() chamado a partir do mesmo.
		EntityManager em = getEntityManager();
		beginTransaction();
		em.merge(t);
		em.getTransaction().commit();
		em.close();
		return t;
	};
	
	public T deletar(ID id)throws MyException{
		//A instancia em da classe EntityManager, recebe o método getEntityManager(), herdado da classe JPAConnection, para que
		//possa atualizar o banco de dados com o método merge() chamado a partir do mesmo.
		EntityManager em = getEntityManager();
		T t = em.find(entidade, id);
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
		
		return t;
	};
	
	public List<T> buscarTodos()throws MyException{
		//Para efetuar a consulta ao banco de dados é feita a utilização da classe TypedQuery, que pode receber tipos genéricos,
		// o que facilita na hora de determinar o tipo do retorno que é recebido.
		
		//Neste caso, o TypedQuery<T> recebe a entidade(objeto/pojo) que é passada pela interface(interface JPADAO)que estends a
		//JPAAbstract
		
		/*Depois do JPAAbstract receber a entidade, o seu construtor faz um espelhamento para que possa inicializar uma variavel com o 
		 seu .class, usado na consulta a seguir, sendo que para isso é feita a utilização do método createQuery(), da classe EntityManager
		recebendo a entidade e recuperando o seu nome, que dependendo das anotações utilizadas, permanece o mesmo.
		No final uma lista (List<T>) recebendo um tipo genérico é montado com o retorno da cunsulta feita.*/ 
		
		TypedQuery<T> consulta=super.getEntityManager().createQuery("SELECT q from "+entidade.getSimpleName()+" q",entidade);
		List<T> retorno=consulta.getResultList();
		return retorno;
	};
}
