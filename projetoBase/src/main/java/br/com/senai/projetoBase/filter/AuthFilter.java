package br.com.senai.projetoBase.filter;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import br.com.senai.projetoBase.exceptions.MyException;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;


/**Classe responsável por validar o tipo de autorização enviada pelo navegador no momento em que efetuou uma requisição ao sistema.
 * Esta autorização deve ser enviada junto do header(cabeçalho) da requisição, junto de um token para garantir a segurança dos dados manipulados.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthFilter implements ContainerRequestFilter{

	
	/**Método responsável por validar se o padrão de autorização enviado pelo navegador está correto. 
	 * 
	 * @param requestContext - Parâmetro que recebe as configurarções da requisição efetuada pelo navegador.
	 */
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

	//Verifica se a requsição HTTP feita tem a autorização necessária para seguir com o procedimento
		String authorizationHeader=requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		
	//Se a requisição HTTP existir, será verificado se está configurado corretamente, 
	//vendo se esta com algum padrão de autorização.Ex:Authorization : Bearer
		if(authorizationHeader  ==  null || !authorizationHeader.startsWith("Bearer ")){
			throw new NotAuthorizedException("A autorização deve ser enviada de maneira correta pelo cabeçalho da aplicação");
		}
		
	//Retira as informações do TOKEN que foi enviado pelo cabeçalho da requisição
		String token=authorizationHeader.substring("Bearer".length()).trim();
		
		
		try{
			//Valida se o token esta correto
			validateToken(token);
		}catch(Exception e){
			requestContext.abortWith(
					Response.status(Response.Status.UNAUTHORIZED).build());
		}
	
	}
	
	/**Método responsável por validar se o padrão de autorização enviado pelo navegador está correto. 
	 * 
	 * @param requestContext - Parâmetro que recebe as configurarções da requisição efetuada pelo navegador.
	 */
	private void validateToken(String token)throws Exception{
		
		final String secret = "dinheiro";
		
		try{
			final JWTVerifier verifier=new JWTVerifier(secret);
			final Map<String,Object>claims=verifier.verify(token);
		}catch(JWTVerifyException e){
			//new AuthException("teste",e);
			new MyException("teste",e);
		}
		
	}

}
