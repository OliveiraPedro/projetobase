package br.com.senai.projetoBase.exceptions;

/**Classe utilizada para efetuar o tratamento de erros que venham a acontecer durante a autenticação do usuário.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class MyLoginException extends Exception {
	
	/**Construtor responsável por chamar as configurações do construtor da classe pai, ou seja, para que seja possível fazer uso
	 *  de uma assinatura adequada para configurar uma mensagem .
	 * 
	 * Pode ser chamado com diversas implementações e com diferentes parâmetros na sua declaração.
	 */
	public MyLoginException(){
		super();
	}
	
	/**Construtor responsável por chamar as configurações do construtor da classe pai, ou seja, para que seja possível fazer uso
	 *  de uma assinatura adequada para configurar uma mensagem .
	 * 
	 * @param mensagem - Recebe como parâmetro um texto que foi enviado no momento em for chamado pela declaração throw new.
	 */
	public MyLoginException(String mensagem){
		super(mensagem);
	}

}
