package br.com.senai.projetoBase.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;













import br.com.senai.projetoBase.filter.Secured;
import br.com.senai.projetoBase.model.Consulta;
import br.com.senai.projetoBase.service.ConsultaService;
import br.com.senai.projetoBase.service.MedicoService;
import br.com.senai.projetoBase.service.PacienteService;


/**Classe REST que define o estilo de arquitetura cliente-servidor para a entidade Consulta.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
@Path("consultas")
@Consumes("application/json")
@Produces("application/json")
public class ConsultaRest extends UtilRest{
	
	@ApiOperation(response = Consulta.class,value = "Cadastrar (CREATE) uma nova consulta/agendamento", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 200, message = "Registro efetuado com sucesso."),
				   @ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Falha no servidor.")})
	
	@POST
	@Secured
	public Response registrarConsulta(@ApiParam(value="Objeto contendo informações para efetuar o cadastro de uma consulta/agendamento", name="json", required=true)String json){
		
		try{
			Consulta consulta = getObjectMapper().readValue(json, Consulta.class);
			ConsultaService service = new ConsultaService();
			
			return buildSaveResponse(service.save(consulta));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Consulta.class,value = "Recuperar (READ) as informações das consultas/agendamentos que estão no sistema",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Não foi possível efetuar sua requisição.")})
	
	@GET
	@Secured
	public Response listarConsultas(){
		
		try{
			ConsultaService service = new ConsultaService();
			
			return buildGetResponse(service.list());
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Consulta.class,value = "Recuperar (READ) informações específicas sobre uma consulta/agendamento em sua entidade.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Falha no servidor.")})
	
	@GET
	@Secured
	@Path("/{id}")
	public Response listarConsultasID(@ApiParam(value="Identificação recebida como parâmetro para efetuar uma busca específica na entidade Consulta", name="id", required=true)@PathParam("id")int id){
		
		try{
			ConsultaService service = new ConsultaService();
			
			return buildGetResponse(service.listID(id));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Consulta.class,value = "Atualizar (UPDATE) as informações de uma consulta/agendamento no sistema", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 200, message = "Registro atualizado com sucesso."),
				   @ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Falha no servidor.")})

	@PUT
	@Secured
	@Path("/{id}")
	public Response editarConsulta(@ApiParam(value="Objeto contendo informações para efetuar a alteração de uma consulta/agendamento", name="json", required=true)String json){
		
		try{
			Consulta consulta = getObjectMapper().readValue(json, Consulta.class);
			ConsultaService service = new ConsultaService();
			
			//O método chamado para atualizar será o mesmo do registro, o que vai comparar no service se será um cadastro
			// ou uma atualização será o id da entidade,se o objeto não tiver o id, será cadastro se tiver será uma atualização
			// de dados
			return buildUpdateResponse(service.save(consulta));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	@ApiOperation(response = Consulta.class,value = "Remover (REMOVE) informações sobre uma consulta/agendamento do sistema.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 200, message = "Registro excluído com sucesso."),
				   @ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Falha no servidor.")})
	
	@DELETE
	@Secured
	@Path("/{id}")
	public Response removerConsulta(@ApiParam(value="Identificação recebida como parâmetro para remover uma consulta/agendamento do sistema", name="id", required=true)@PathParam("id")int id){
		
		try{
			ConsultaService service = new ConsultaService();
			
			return buildRemoveResponse(service.delete(id));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	/************AUTOCOMPLETE SEARCH*************/
	
	
	/************PACIENT AUTO*************/
	
	@ApiOperation(response = Consulta.class,value = "Recuperar (READ) informações específicas sobre um paciente em sua entidade.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Falha no servidor.")})
	
	@GET
	@Secured
	@Path("/listarPacientesAC")
	public Response listarPacientesAC(@ApiParam(value="Nome do paciente recebido como parâmetro para efetuar uma busca específica em sua entidade", name="paciente", required=true)@QueryParam("search") String paciente){
		
		try{
			PacienteService service=new PacienteService();
			
			return buildGetResponse(service.listPatient(paciente));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
	/************MEDIC AUTO*************/
	@ApiOperation(response = Consulta.class,value = "Recuperar (READ) informações específicas sobre um médico em sua entidade.",produces = MediaType.APPLICATION_JSON)
	@ApiResponses({@ApiResponse(response = Consulta.class, code = 404, message = "Página não encontrada."),
				   @ApiResponse(response = Consulta.class, code = 500, message = "Falha no servidor.")})
	
	@GET
	@Secured
	@Path("/listarMedicosAC")
	public Response listarMedicosAC(@ApiParam(value="Nome do medico recebido como parâmetro para efetuar uma busca específica em sua entidade", name="medico", required=true)@QueryParam("search") String medico){
		
		try{
			MedicoService service=new MedicoService();
			
			return buildGetResponse(service.listMedic(medico));
		}catch(Exception e){
			e.printStackTrace();
			return buildErrorResponse(e);
		}
	}
	
}
