package br.com.senai.projetoBase.service;

import java.util.List;

import br.com.senai.projetoBase.dao.MedicoDAO;
import br.com.senai.projetoBase.dao.PacienteDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.factory.DAOFactory;
import br.com.senai.projetoBase.model.Medico;
import br.com.senai.projetoBase.model.Paciente;

/**Classe responsável por efetuar a troca de informações sobre a entidade Medico entre a camada REST e o servidor.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class MedicoService {

	public Medico save(Medico m) throws MyException {
		//É passado para o objeto persist a instancia do método static getInstanceOf(), que retorna uma interface que contém acesso 
		//aos métodos de persistencia
		MedicoDAO persist= (MedicoDAO) DAOFactory.getInstanceOf(MedicoDAO.class);
		return persist.incluir(m);
	}

	public List<Medico> list() throws MyException {
		MedicoDAO persist= (MedicoDAO) DAOFactory.getInstanceOf(MedicoDAO.class);
		return persist.buscarTodos();
	}

	public Medico listID(int id) throws MyException {
		MedicoDAO persist= (MedicoDAO) DAOFactory.getInstanceOf(MedicoDAO.class);
		return persist.buscarPorId(id);
	}

	public Medico update(Medico m) throws MyException {
		MedicoDAO persist= (MedicoDAO) DAOFactory.getInstanceOf(MedicoDAO.class);
		return persist.atualizar(m);
	}

	public Medico delete(int id) throws MyException {
		MedicoDAO persist= (MedicoDAO) DAOFactory.getInstanceOf(MedicoDAO.class);
		return persist.deletar(id);
	}

	public List<Medico> listMedic(String medico) throws MyException {
		MedicoDAO persist= (MedicoDAO) DAOFactory.getInstanceOf(MedicoDAO.class);
		return persist.listarMedicosAC(medico);
	}

}
