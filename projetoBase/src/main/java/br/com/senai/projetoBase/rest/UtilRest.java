package br.com.senai.projetoBase.rest;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import br.com.senai.projetoBase.exceptions.MyException;

/**Classe responsável pelos métodos que retornam mensagens personalizadas de sucesso/erro após uma requisição efetuada.
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class UtilRest {

	/**Método responsável por formatar a data enviada no momento da requisição ao servidor.
	 * 
	 * @return dateFormat - Retorna um objeto contendo a data no formato yyyy-MM-dd.
	 *
	 */
	protected ObjectMapper getObjectMapper(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return new ObjectMapper()
				.setDateFormat(dateFormat)
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
				.configure(SerializationFeature.INDENT_OUTPUT, true)
				.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
	}
	
	/**Método responsável por retornar um objeto contendo uma resposta de sucesso ao efetuar um cadastro(CREATE).
	 * 
	 * @param obj - Objeto contendo a resposta de sucesso do servidor.
	 * 
	 * @return RestResponse - Retorna um objeto a resposta formatada, com seu status e mensagem personalizada.
	 *
	 */
	protected Response buildSaveResponse(Object obj){
		return  buildResponse(new RestResponse(
					Response.Status.CREATED.getStatusCode(),
					"success",
					"Registro inserido com sucesso",
					obj
				));
	}
	
	/**Método responsável por retornar um objeto contendo uma resposta de sucesso ao efetuar uma alteração(UPDATE).
	 * 
	 * @param obj - Objeto contendo a resposta de sucesso do servidor.
	 * 
	 * @return RestResponse - Retorna um objeto a resposta formatada, com seu status e mensagem personalizada.
	 *
	 */
	protected Response buildUpdateResponse(Object obj){
		return  buildResponse(new RestResponse(
					Response.Status.OK.getStatusCode(),
					"success",
					"Registro atualizado com sucesso",
					obj
				));
	}
	
	/**Método responsável por retornar um objeto contendo uma resposta de sucesso ao efetuar uma remoção(REMOVE).
	 * 
	 * @param obj - Objeto contendo a resposta de sucesso do servidor.
	 * 
	 * @return RestResponse - Retorna um objeto a resposta formatada, com seu status e mensagem personalizada.
	 *
	 */
	protected Response buildRemoveResponse(Object obj){
		return  buildResponse(new RestResponse(
					Response.Status.OK.getStatusCode(),
					"success",
					"Registro excluido com sucesso",
					obj
				));
	}
	
	/**Método responsável por retornar um objeto contendo uma resposta de sucesso ao efetuar uma consulta(READ).
	 * 
	 * @param obj - Objeto contendo a resposta de sucesso do servidor.
	 * 
	 * @return RestResponse - Retorna um objeto a resposta formatada, com seu status e mensagem personalizada.
	 *
	 */
	protected Response buildGetResponse(Object obj){
		return  buildResponse(new RestResponse(
					Response.Status.OK.getStatusCode(),
					"success",
					null,
					obj
				));
	}
	
	/**Método responsável por construir a resposta de sucesso (SUCCESS) conforme os parametros que recebe ao ser chamado pelo outros métodos da classe.
	 * 
	 * @param obj - Objeto contendo o tipo de retorno que deseja usar na requisição.
	 * 
	 * @return RestResponse - Retorna um objeto com a resposta de sucesso formatada, seu status e mensagem personalizada.
	 *
	 */
	protected Response buildResponse(Object obj){
		String json = null;
		try {
			json = getObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return buildErrorResponse(e);
		}
		return Response.ok(json).build();
	}

	
	/**Método responsável por retornar a estrutura da mensagem de erro quando é solicitada uma requisição a um endereço inexistente (erro 400).
	 *
	 *@param e - Parâmetro que contém a exceção gerada ao efetuar uma requisição a um endereço inexistente.
	 *
	 *@return e - Retorna um objeto contendo a exceção(e o seo código) gerada depois de efetuar uma requisição a um endereço inexistente.
	 */
	protected Response buildErrorResponse(MyException e) {
		return buildErrorResponse(e, Response.Status.BAD_REQUEST);
	}

	/**Método responsável por retornar a estrutura da mensagem de erro quando ocorre um erro inesperado no servidor (erro 500).
	 *
	 *@param e - Parâmetro que contém a exceção gerada ao ocorrer um erro inesperado no servidor
	 *
	 *@return e - Retorna um objeto contendo a exceção(e o seo código) gerada depois de ocorrer um erro inesperado no servidor.
	 */
	protected Response buildErrorResponse(Exception e) {
		return buildErrorResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
	}

	/**Método responsável por construir a resposta de erro (ERROR) conforme os parametros que recebe ao ser chamado pelo outros métodos da classe.
	 * 
	 * @param obj - Objeto contendo o tipo de retorno que deseja usar na requisição.
	 * 
	 * @return RestResponse - Retorna um objeto com a resposta de erro formatada, seu status e mensagem personalizada.
	 *
	 */
	protected Response buildErrorResponse(Exception e, Response.Status status) {
		String json = null;
		try {
			json = getObjectMapper().writeValueAsString(new RestResponse(e));
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
			return null;
		}
		return Response.status(status)
				.entity(json)
				.build();
	}
}
