package br.com.senai.projetoBase.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.senai.projetoBase.abs.JPAAbstract;
import br.com.senai.projetoBase.dao.UsuarioDAO;
import br.com.senai.projetoBase.exceptions.MyException;
import br.com.senai.projetoBase.model.Usuario;
/**Classe que que utiliza os métodos específicos de persistência a entidade Usuario
 * 
 * @version 1.0.0
 * @since 1.0.0
 * 
 * @author Pedro Henrique de Oliveira Câmpelo
 *
 */
public class JPAUsuarioDAO extends JPAAbstract<Usuario,Integer> implements UsuarioDAO{

	@Override
	public boolean findUser(String usuario, String senha) throws MyException {
		System.out.println("usuario v2");
		List<Usuario> tarefas=null;
		try {
			EntityManager em = getEntityManager();
			TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE upper(u.usuario) = upper(:usuario) AND upper(u.senha) = upper(:senha)", Usuario.class);
			query.setParameter("usuario", usuario);
			query.setParameter("senha", senha);
			
			if(query.getResultList().size()>0){
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			throw new MyException(e);
		} finally {
			closeEntityManager();
		}
	}

}