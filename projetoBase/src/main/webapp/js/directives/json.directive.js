/**
 * 
 */
(function(){
	'use strict';
	
	angular.module("pro_clinica").directive("json",Directive);
	
	//Iniciando uma injeção de dependencia de lista para a função Directive, pois a mesma
	// ira retornar uma string json contendo uma lista de valores para exibir na tela
	
	Directive.$inject=[];
	
	function Directive(){
		
		return{
			//Permite com que a diretiva json seja usada como tag HTML na camada view
			restrict:'E',
			scope:{
				//Cria o atributo data para a diretiva json, que recupera o valor da variavel tarefa, 
				//recuperada do controller de tarefas
				'data':'=data'
			},
			templateUrl:'js/directives/json.html'
		};
	}
	
})();