/**
 * 
 */
(function(){
	'use strict';
	
	angular.module("pro_clinica").directive("capitalize",Directive);
	
	//Iniciando uma injeção de dependencia de lista para a função Directive, pois a mesma
	// ira retornar uma string json contendo uma lista de valores para exibir na tela
	
	Directive.$inject=["uppercaseFilter","lowercaseFilter"];
	
	function Directive(uc,lc){
		
		return{
			restrict:'A',
			require:'ngModel',
			
			link:function(scope,element,attrs,ngModel){
				
				var formato=/\b[a-z]/g;
				
				ngModel.$parsers.push(capitalize);
				debugger;
				
				function capitalize(value){
					if(formato.test(value)){
						value=lc(value).replace(formato,uc);
					}
					return null;
				}
				
				
			}
		};
	}
	
})();