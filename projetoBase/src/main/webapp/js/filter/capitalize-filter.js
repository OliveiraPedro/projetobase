/**
 * 
 */
(function(){
	'use strict'

	angular.module("pro_clinica").filter("capitalize", Filter);

	Filter.$inject=["uppercaseFilter","lowercaseFilter"];

	function Filter(uc,lc){


		return function(input){

			if(input){
				input =lc(input).replace(/\b[a-z]/g, function(letter){
					return uc(letter);
				});
			}

			return input;	
		};


	}
})();
