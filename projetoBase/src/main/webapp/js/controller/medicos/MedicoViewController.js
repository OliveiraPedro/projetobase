/**
 * 
 */
(function(){
	'use strict';

	//Declaração modificada do controller medicoController.Foi modificado
	// para que puesse implementar uma ou mais dependencias
	angular.module("pro_clinica").controller("MedicoViewController",Controller);


	Controller.$inject=['MedicoFactory','toaster','$state','$localStorage','AuthService'];


	//Função privada Controller que efetua a persistencia de dados.
	//Recebe como parametro lc, que guarda as configurações de filtro do AngularJS, que foi especificado
	// na injeção de uma depdendencia quando o controller foi criado..
	function Controller(MedicoFactory,toaster,$state,$localStorage,AuthService){
		//Váriavel criada para que seja usada de referencia ao criar funções privadas
		//juntamente com outros tipos de métodos
		var self=this;

		//Lista que guarda as medicos cadastradas
		self.medicos=[];


		//Recuperar todas as medicos disponiveis no banco de dados
		MedicoFactory.get().then(function(data){
			if(data.data){
				self.medicos=data.data;
			}
			/**/debugger;
		});



		//Função que busca no banco de dados a medico pelo valor especificado no campo de pesquisa
		self.buscarMedico=function(pesquisa){
			MedicoFactory.search(pesquisa).then(function(data){
				self.medicos=data.data;
			});
		}


		//Método recebe o parametro da medico selecionada no momento da exclusão
		debugger;
		self.removerMedico=function(medico){
			MedicoFactory.remove(medico.id).then(function(data){

				toaster.pop(data.status, "Médico", data.mensagem);
				//Váriavel que recebe um valor negativo que é utilizada para guardar a posição do item passado por parametro,se existir
				var pos=-1;
				debugger;
				//A função forEach, semelhante a função homônima(mesmo nome) do javascript para percorrer a lista de medicos

				//O laço forEach recebe como parametros a lista de medicos(que recebe seu valor no momento do cadastro) 
				//alem de uma função construtora que recebe o item selecionado e sua posição

				//Após isso é feita a verificação de cada elemento da lista, comparando se as identificações(id) são iguais
				//Sendo iguais a váriavel pos recebe o valor do indice(posição) do item selecionado.

				//A partir deste momento a variavel pos se torna o id do item selecionado(somente para verificações)
				angular.forEach(self.medicos,function(item,index){
					if(medico.id==item.id){
						//Armazena o indice do parametro recebido caso o id dos atributos da lista de medicos sejam iguais
						pos=index;
					}
				});

				//A partir deste momento, a variavel pos sempre será maior que -1,devido a mesma receber o indice do parametro recebido
				//É chamado o método splice da lista de medicos para remover o item da posição/indice pos.Recebe como parametros a posição selecionada
				// e o quantidade a ser removida,neste caso 1


				//Como a variavel pos,já tendo recebido o valor da posição do item na lista, sempre será maior, o método splice será chamado
				//recebendo como parametros pos(posicao do item) e quantidade que será removida do array, que neste caso será somente 1(hum)
				if(pos>-1){
					self.medicos.splice(pos,1);
					debugger;
				}
			}).catch(function(error){
				toaster.pop(error.data.status, "Médico", error.data.mensagem);
			});
		}
		debugger;
		//Altera os valores da variavel medico sempre que for chamado, para que possa preencher o formulário com valores já existentes
		self.selecionarMedico=function(medico){
			//A função copy, do angular, envita que os valores digitados no formulários reflitam(interfiram) 
			//diretamente no resultado que foi selecionado da lista,pois estamos utilizando o mesmo objeto medico,
			//ou seja tanto quanto o cadastro(CREATE) quanto a atualização(UPDATE) estão utilizando a mesma referencia do controller.medico
			$state.go("private.editarMedico",{id:medico.id});
		}

		self.logOut=function(){
			AuthService.Logout();
		}




	}
})();