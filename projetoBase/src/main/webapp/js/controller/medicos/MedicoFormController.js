/**
 * 
 */
(function(){
	'use strict';
	
	//Declaração modificada do controller medicoController.Foi modificado
	// para que puesse implementar uma ou mais dependencias
	angular.module("pro_clinica").controller("MedicoFormController",Controller);
	
	Controller.$inject=['MedicoFactory','toaster','$state','$stateParams'];

	function Controller(MedicoFactory,toaster,$state,$stateParams){

		var self=this;

		self.medico={};

		self.salvarMedico=function(medico){
			debugger;
			if(medico.id){
				editarMedico(medico)
			}else{
				inserirMedico(medico);
			}
			debugger;
		}
		
		function inserirMedico(medico){	
			MedicoFactory.save(medico).then(function(data){
				if(data.data){	
					$state.go("private.medicos");
					toaster.pop(data.status, "Médico", data.mensagem);
				}
            }).catch(function(error){
            	toaster.pop(error.data.status, "Médico", error.data.mensagem);
            });
			
		}	
		
		function editarMedico(medico){
			
			MedicoFactory.update(medico.id,medico).then(function(data){
				
				$state.go("private.medicos");
				toaster.pop(data.status, "Médico", data.mensagem);
               
			}).catch(function(error){
            	toaster.pop(error.data.status, "Médico", error.data.mensagem);
            });
		}
		debugger;
		function init(){
			debugger;
			if($stateParams.id){
				MedicoFactory.get($stateParams.id).then(function(data){
					if(data.data){
					self.medico=data.data;
					} else{
						$state.go('private.medicos');
					}
				});
			}
		}
		debugger;
		init();
	}
})();