/**
 * 
 */
(function(){
	'use strict';
	
	//Declaração modificada do controller pacienteController.Foi modificado
	// para que puesse implementar uma ou mais dependencias
	angular.module("pro_clinica").controller("ConsultaFormController",Controller);
	
	Controller.$inject=['ConsultaFactory','PacienteFactory','MedicoFactory','toaster','$state','$stateParams','$scope'];

	function Controller(ConsultaFactory,PacienteFactory,MedicoFactory,toaster,$state,$stateParams,$scope){

		var self=this;

		self.consulta = {};
		
		self.consultas=[];
		self.medicos=[];
		self.pacientes=[];
		
		self.paciente={};
		self.medico={};
	
			

		self.salvarConsulta=function(consulta){
			
			if(consulta.id){
				editarConsulta(consulta)
			}else{
				inserirConsulta(consulta);
			}
			
		}
		
		function inserirConsulta(consulta){	
			
			alert(consulta);
			console.log(consulta);
			
			console.log("------COMECOU O PACIENTE----");
			console.log(consulta);

			ConsultaFactory.save(consulta).then(function(data){
				if(data.data){	
					$state.go("private.consultas");
					toaster.pop(data.status, "Consulta", data.mensagem);
				}
            }).catch(function(error){
            	toaster.pop(error.data.status, "Consulta", error.data.mensagem);
            });
			
		}	
		
		function editarConsulta(consulta){
			
			ConsultaFactory.update(consulta.id,consulta).then(function(data){
				
				$state.go("private.consultas");
				toaster.pop(data.status, "Consulta", data.mensagem);
               
			}).catch(function(error){
            	toaster.pop(error.data.status, "Consulta", error.data.mensagem);
            });
		}
		
		function init(){
			
			if($stateParams.id){
				ConsultaFactory.get($stateParams.id).then(function(data){
					if(data.data){
					self.consulta=data.data;
					} else{
						$state.go('private.consultas');
					}
				});
			}
		}
		init();
		debugger;
		function initPatients(){
			PacienteFactory.get().then(function(data){
				self.pacientes=data.data;
				//self.consulta.paciente=data.data;
			});
		}
		initPatients();
		
		function initDoctors(){
			MedicoFactory.get().then(function(data){
				self.medicos=data.data;
				//self.consulta.paciente=data.data;
			});
		}
		initDoctors();
		
	}
		})();