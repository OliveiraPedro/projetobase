/**
 * 
 */
(function(){
	'use strict';

	angular.module("pro_clinica").controller("ConsultaViewController",Controller);


	Controller.$inject=['ConsultaFactory','toaster','$state','$localStorage','AuthService'];


	function Controller(ConsultaFactory,toaster,$state,$localStorage,AuthService){


		var self=this;

		//Lista que guarda as consultas cadastradas
		self.consultas=[];


		//Recuperar todas as consultas disponiveis no banco de dados
		ConsultaFactory.get().then(function(data){
			if(data.data){
				self.consultas=data.data;
			}
		});



		//Função que busca no banco de dados a consulta pelo valor especificado no campo de pesquisa
		self.buscarConsulta=function(pesquisa){
			ConsultaFactory.search(pesquisa).then(function(data){
				self.consultas=data.data;
			});
		}


		//Método recebe o parametro da consulta selecionada no momento da exclusão
		debugger;
		self.removerConsulta=function(consulta){
			debugger;
			ConsultaFactory.remove(consulta.id).then(function(data){

				toaster.pop(data.status, "Consulta", data.mensagem);

				var pos=-1;

				
				angular.forEach(self.consultas,function(item,index){
					if(consulta.id==item.id){
						//Armazena o indice do parametro recebido caso o id dos atributos da lista de consultas sejam iguais
						pos=index;
					}
				});


				if(pos>-1){
					self.consultas.splice(pos,1);
					debugger;
				}
			}).catch(function(error){
				toaster.pop(error.data.status, "Consulta", error.data.mensagem);
			});
		}
		debugger;
		
		self.selecionarConsulta=function(consulta){
			$state.go("private.editarConsulta",{id:consulta.id});
		}




	}
})();