/**
 * 
 */
(function(){
	'use strict';

	//Declaração modificada do controller pacienteController.Foi modificado
	// para que puesse implementar uma ou mais dependencias
	angular.module("pro_clinica").controller("PacienteViewController",Controller);


	Controller.$inject=['PacienteFactory','toaster','$state','$localStorage','AuthService'];


	//Função privada Controller que efetua a persistencia de dados.
	//Recebe como parametro lc, que guarda as configurações de filtro do AngularJS, que foi especificado
	// na injeção de uma depdendencia quando o controller foi criado..
	function Controller(PacienteFactory,toaster,$state,$localStorage,AuthService){
		//Váriavel criada para que seja usada de referencia ao criar funções privadas
		//juntamente com outros tipos de métodos
		var self=this;

		//Lista que guarda as pacientes cadastradas
		self.pacientes=[];


		//Recuperar todas as pacientes disponiveis no banco de dados
		PacienteFactory.get().then(function(data){
			if(data.data){
				self.pacientes=data.data;
			}

			/**/
		});



		//Função que busca no banco de dados a paciente pelo valor especificado no campo de pesquisa
		self.buscarPaciente=function(pesquisa){
			PacienteFactory.search(pesquisa).then(function(data){
				self.pacientes=data.data;
			});
		}


		//Método recebe o parametro da paciente selecionada no momento da exclusão
		debugger;
		self.removerPaciente=function(paciente){
			PacienteFactory.remove(paciente.id).then(function(data){

				toaster.pop(data.status, "pacientes", data.mensagem);
				//Váriavel que recebe um valor negativo que é utilizada para guardar a posição do item passado por parametro,se existir
				var pos=-1;
				debugger;
				//A função forEach, semelhante a função homônima(mesmo nome) do javascript para percorrer a lista de pacientes

				//O laço forEach recebe como parametros a lista de pacientes(que recebe seu valor no momento do cadastro) 
				//alem de uma função construtora que recebe o item selecionado e sua posição

				//Após isso é feita a verificação de cada elemento da lista, comparando se as identificações(id) são iguais
				//Sendo iguais a váriavel pos recebe o valor do indice(posição) do item selecionado.

				//A partir deste momento a variavel pos se torna o id do item selecionado(somente para verificações)
				angular.forEach(self.pacientes,function(item,index){
					if(paciente.id==item.id){
						//Armazena o indice do parametro recebido caso o id dos atributos da lista de pacientes sejam iguais
						pos=index;
					}
				});

				//A partir deste momento, a variavel pos sempre será maior que -1,devido a mesma receber o indice do parametro recebido
				//É chamado o método splice da lista de pacientes para remover o item da posição/indice pos.Recebe como parametros a posição selecionada
				// e o quantidade a ser removida,neste caso 1


				//Como a variavel pos,já tendo recebido o valor da posição do item na lista, sempre será maior, o método splice será chamado
				//recebendo como parametros pos(posicao do item) e quantidade que será removida do array, que neste caso será somente 1(hum)
				if(pos>-1){
					self.pacientes.splice(pos,1);
					debugger;
				}
			}).catch(function(error){
				toaster.pop(error.data.status, "pacientes", error.data.mensagem);
			});
		}
		debugger;
		//Altera os valores da variavel paciente sempre que for chamado, para que possa preencher o formulário com valores já existentes
		self.selecionarPaciente=function(paciente){
			//A função copy, do angular, envita que os valores digitados no formulários reflitam(interfiram) 
			//diretamente no resultado que foi selecionado da lista,pois estamos utilizando o mesmo objeto paciente,
			//ou seja tanto quanto o cadastro(CREATE) quanto a atualização(UPDATE) estão utilizando a mesma referencia do controller.paciente
			$state.go("private.editarPaciente",{id:paciente.id});
		}

		self.logOut=function(){
			AuthService.Logout();
		}




	}
})();