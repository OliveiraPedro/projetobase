/**
 * 
 */
(function(){
	'use strict';
	
	//Declaração modificada do controller pacienteController.Foi modificado
	// para que puesse implementar uma ou mais dependencias
	angular.module("pro_clinica").controller("PacienteFormController",Controller);
	
	Controller.$inject=['PacienteFactory','toaster','$state','$stateParams'];

	function Controller(PacienteFactory,toaster,$state,$stateParams){

		var self=this;

		self.paciente={};

		self.salvarPaciente=function(paciente){
			debugger;
			if(paciente.id){
				editarPaciente(paciente)
			}else{
				inserirPaciente(paciente);
			}
			debugger;
		}
		
		function inserirPaciente(paciente){	
			PacienteFactory.save(paciente).then(function(data){
				if(data.data){	
					$state.go("private.pacientes");
					toaster.pop(data.status, "Pacientes", data.mensagem);
				}
            }).catch(function(error){
            	toaster.pop(error.data.status, "Pacientes", error.data.mensagem);
            });
			
		}	
		
		function editarPaciente(paciente){
			
			PacienteFactory.update(paciente.id,paciente).then(function(data){
				
				$state.go("private.pacientes");
				toaster.pop(data.status, "Pacientes", data.mensagem);
               
			}).catch(function(error){
            	toaster.pop(error.data.status, "Pacientes", error.data.mensagem);
            });
		}
		debugger;
		function init(){
			debugger;
			if($stateParams.id){
				PacienteFactory.get($stateParams.id).then(function(data){
					if(data.data){
					self.paciente=data.data;
					} else{
						$state.go('view');
					}
				});
			}
		}
		debugger;
		init();
	}
})();