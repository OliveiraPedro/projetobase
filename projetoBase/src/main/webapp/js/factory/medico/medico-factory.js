/**
 * 
 */
(function(){
	angular.module("pro_clinica").factory("MedicoFactory",function($resource){
		var factory = {
				get : fnGet,
				save : fnSave,
				update : fnUpdate,
				search : fnSearch,
				remove : fnRemove
		}
		var resource = $resource("/projetoBase/rest/medicos/:id", null, {
			'update' : {
				method : 'PUT'
			},
			'search' : {
				method : 'GET',
				params : {
					query : '@query'
				}
			}

		});
	
		//$promise:representa o estado do retorno do método HTTP que é utilizado.
		//Pode retornar .then(), utilizado para tratar uma requisição feita com sucesso
		// e .catch(), que recupera a resposta da requisição se acontecer algum erro
	function fnGet(id){
		if(id){
			return resource.get({id:id}).$promise;
		} else {
			return resource.get().$promise;
		}
	}
	
	function fnSave(data){
		return resource.save(data).$promise;
	}
	debugger;
	function fnUpdate(id,data){
		return resource.update({id:id},data).$promise;
	}
	
	function fnSearch(param){
		return resource.search({search:param}).$promise;
	}
	
	function fnRemove(id){
		return resource.delete({id:id}).$promise;
	}
	
	return factory;

	});
})();


