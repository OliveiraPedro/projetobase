/**
 * 
 */
(function(){
	angular.module("pro_clinica").factory("LoginFactory",function($resource){
		var factory = {
				save : fnSave
		}
		var resource = $resource("/projetoBase/rest/usuarios/", null, {
			'update' : {
				method : 'PUT'
			},
			'search' : {
				method : 'GET',
				params : {
					query : '@query'
				}
			}

		});
	
		//$promise:representa o estado do retorno do método HTTP que é utilizado.
		//Pode retornar .then(), utilizado para tratar uma requisição feita com sucesso
		// e .catch(), que recupera a resposta da requisição se acontecer algum erro
	
		function fnSave(data){
			return resource.save(data).$promise;
		}
	
	
	return factory;

	});
	
	
})();


