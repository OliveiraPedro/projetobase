/**
 * 
 */
(function(){

	angular.module("pro_clinica").config(function($stateProvider,$urlRouterProvider) {

		//$urlRouterProvider:service utilizado para mapear/configurar a rota principal de acesso aos states(estados) para transição de telas
		$urlRouterProvider.otherwise('/welcome');

		//$stateProvider:service utilizado para modificar a URL padrão da aplicação conforme o state(estado) que está sendo acessado.Depende internamente
		//do service $urlRouterProvider para acessar a rota de acesso configurada anteriormente.Alguns dos parametros utilizados são:

		//url:recebe o valor que modifica a url no momento em que o este é acessado
		//templateUrl:especifica para qual página a aplicação é direcionada,definindo a transição de telas da aplicação
		//controller:Define o controller que o state dependera para efetuar as suas operações
		//controllerAs:Define o nome da instancia do controller que o state depende

		$stateProvider.state('private', {
			url: '',
			abstract:true,
			templateUrl: 'js/directives/menu.html',
			controller:'LoginController',
			controllerAs:'controller'
		});		
		

		$stateProvider.state('login', {
			url: '/login',
			templateUrl: 'pages/login/login.html',
			controller:'LoginController',
			controllerAs:'controller'
		});
		
		$stateProvider.state('cadUser', {
			url: '/login/new',
			templateUrl: 'pages/login/form.html',
			controller:'LoginFormController',
			controllerAs:'controller'
		});
		

		$stateProvider.state('private.welcome', {
			url: '/welcome',
			templateUrl: 'js/directives/welcome.html',
			controller:'LoginController',
			controllerAs:'controller'
		});
		

		$stateProvider.state('private.pacientes', {
			url: '/pacientes',
			templateUrl: 'pages/pacientes/view.html',
			controller:'PacienteViewController',
			controllerAs:'controller',
			//authorize:true
		});
		
		
		$stateProvider.state('private.cadastrarPaciente', {
			url: '/pacientes/cadastro/new',
			templateUrl: 'pages/pacientes/form.html',
			controller:'PacienteFormController',
			controllerAs:'controller'
		});
		
		
		$stateProvider.state('private.editarPaciente', {
			url: '/paciente/edit/:id',
			templateUrl: 'pages/pacientes/form.html',
			controller:'PacienteFormController',
			controllerAs:'controller'
		});
		
		
		$stateProvider.state('private.medicos', {
			url: '/medicos',
			templateUrl: 'pages/medicos/view.html',
			controller:'MedicoViewController',
			controllerAs:'controller'
			//authorize:true
		});
		
		
		$stateProvider.state('private.cadastrarMedico', {
			url: '/medicos/cadastro/new',
			templateUrl: 'pages/medicos/form.html',
			controller:'MedicoFormController',
			controllerAs:'controller'
			//authorize:true
		});
		
		
		$stateProvider.state('private.editarMedico', {
			url: '/medicos/edit/:id',
			templateUrl: 'pages/medicos/form.html',
			controller:'MedicoFormController',
			controllerAs:'controller'
			//authorize:true
		});
		
		
		$stateProvider.state('private.consultas', {
			url: '/consultas',
			templateUrl: 'pages/consultas/view.html',
			controller:'ConsultaViewController',
			controllerAs:'controller',
			//authorize:true
		});
		
		$stateProvider.state('private.cadastrarConsulta', {
			url: '/consulta/new',
			templateUrl: 'pages/consultas/form.html',
			controller:'ConsultaFormController',
			controllerAs:'controller',
			//authorize:true
		});
		
		$stateProvider.state('private.editarConsulta', {
			url: '/consulta/edit/:id',
			templateUrl: 'pages/consultas/form.html',
			controller:'ConsultaFormController',
			controllerAs:'controller',
			//authorize:true
		});
		

	});
	angular.module("pro_clinica").run(Autorizacao);

Autorizacao.$inject=['$rootScope','$state','$localStorage'];

function Autorizacao($rootScope,$state,$localStorage){
	debugger;
	$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
		debugger;
		var loginUrl = "/login";
		console.log($localStorage["token"]+" token atual");
		if(loginUrl !== toState.url){
			// verificar se o token esta no local storage, senao, envia para o login
			
			//Se o usuário não for válido,ou seja não gerar o token, o mesmo não terá autorização para executar as requisições do sistema e será redirecionado
			// para que possa entrar corretamente na aplicação
			
			//Verificar validação de token undefined
			if(!$localStorage["token"]){
				$state.go("login");				
				//Se o usuário for válido, mas por algum acaso tentar acessar a pagina de login denovo, o mesmo será redirecionado para a página principal da aplicação
			}
			
		} else if($localStorage["token"]){
			$state.go("private.welcome");
		}
		
	});
	
}

})();