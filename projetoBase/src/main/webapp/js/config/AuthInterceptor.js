
(function(){
	'use strict';

	angular.module("pro_clinica").factory("Interceptor", Factory);

	Factory.$inject = ['$localStorage', 'toaster','$q'];

	function Factory($localStorage, toaster,$q){
		var httpInterceptor = {
				request: function(config){
					if($localStorage.token){
						config.headers['Authorization'] = 'Bearer ' + $localStorage.token;
					}
					return config || $q.when(config);
				}, response: function(response){	
					if(response.data.status && response.data.message){
						toaster.pop(response.data.status, "Aviso!",response.data.message);
					}
					return response;
				}, responseError: function(response){
					if(response.data.status && response.data.message){
						toaster.pop(response.data.status, "Aviso!",response.data.message);
					}
					return $q.reject(response);
				}
		}	
		return httpInterceptor;

	}

	angular.module("pro_clinica").config(['$httpProvider', function($httpProvider){
		$httpProvider.interceptors.push('Interceptor');
	}]);
})();
