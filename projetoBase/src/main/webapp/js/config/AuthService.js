/**
 * 
 */
(function(){
	'use strict';

	angular.module('pro_clinica')

	.factory('AuthService',['$http','$localStorage','$state','toaster',function ($http,$localStorage,$state,toaster) {
		var AuthService = {};

		AuthService.Login = function (login) {
			$http.post('/projetoBase/rest/endpoint', login).then(function (data) {            	
				if(data.data){
					$localStorage.token=data.data.token;
					$state.go("private.welcome");
					toaster.pop( "success", "Sucesso", "Login efetuado com sucesso!");
				}else{
					console.log("testeeqwgq");
				}
			}).catch(function(error){
				$localStorage.$reset();
				toaster.pop(error.data.status,"Erro",error.data.mensagem);
			});

		};

		AuthService.Logout=function(){
			$localStorage.$reset();
			$state.go("login");
			toaster.pop( "success", "Sucesso", "Logout efetuado com sucesso!");
		};

		return AuthService;
	}]);
})();