/**
 * 
 */
(function(){
	'use strict'
	
	angular.module("pro_clinica",['ngMessages','ngResource','ngAnimate','toaster','ui.router','ngStorage','base64','localytics.directives']);
})();